import React, { Fragment } from "react";
import "./App.css";

//components
import MainPage from "./pages/MainPage";

function App() {
  return (
    <Fragment>
      <div className="container">
        <MainPage />
      </div>
    </Fragment>
  );
}

export default App;
