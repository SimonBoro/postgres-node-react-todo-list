import React, { Fragment, useState, useEffect } from "react";

//components
import InputTodo from "../components/InputTodo";
import ListTodo from "../components/ListTodo";
import ListDone from "../components/ListDone";

const MainPage = () => {
  const [todosChange, setTodosChange] = useState(false);
  const [todoTask, setTodoTask] = useState([]);
  const [doneTask, setDoneTask] = useState([]);

  //get all todos
  const getTodos = async () => {
    const response = await fetch("http://localhost:5000/todos");

    //convert response to json
    const jsonData = await response.json();

    //set todo and done data
    setTodoTask(jsonData.filter((todo) => todo.isdone !== true));
    setDoneTask(jsonData.filter((todo) => todo.isdone !== false));
  };

  // delete todo
  const deleteTodo = async (id) => {
    try {
      const deleteTodo = await fetch(`http://localhost:5000/todos/${id}`, {
        method: "DELETE",
      });

      setTodosChange(true);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getTodos(); //update todos
    setTodosChange(false); //set back setTodosChange as false
  }, [todosChange]); //watch changing in todosChange

  return (
    <Fragment>
      <h1 className="text-center mt-5">Simon - Todo List</h1>
      <InputTodo setTodosChange={setTodosChange} />
      <ListTodo
        todoTask={todoTask}
        setTodosChange={setTodosChange}
        deleteTodo={deleteTodo}
      />
      <ListDone
        doneTask={doneTask}
        setTodosChange={setTodosChange}
        deleteTodo={deleteTodo}
      />
    </Fragment>
  );
};

export default MainPage;
