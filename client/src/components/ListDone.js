import React, { Fragment } from "react";

//components
import EditTodo from "./EditTodo";

const ListDone = ({ doneTask, setTodosChange, deleteTodo }) => {
  //mark task as undone
  const markAsUndone = async (id) => {
    try {
      const response = await fetch(`http://localhost:5000/todos/undone/${id}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
      });

      setTodosChange(true);
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      <h3 className="mt-5">Task done:</h3>
      <table className="table text-center">
        <thead>
          <tr>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
            <th>Mark as Undone</th>
          </tr>
        </thead>
        <tbody>
          {doneTask.map((d_task) => (
            <tr key={d_task.todo_id}>
              <td>{d_task.description}</td>
              <td>
                <EditTodo todo={d_task} setTodosChange={setTodosChange} />
              </td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => deleteTodo(d_task.todo_id)}
                >
                  Delete
                </button>
              </td>
              <td>
                <button
                  className="btn btn-secondary"
                  onClick={() => markAsUndone(d_task.todo_id)}
                >
                  Undone
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Fragment>
  );
};

export default ListDone;
