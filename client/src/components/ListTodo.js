import React, { Fragment } from "react";

//components
import EditTodo from "./EditTodo";

const ListTodo = ({ todoTask, setTodosChange, deleteTodo }) => {
  //mark task as done
  const markAsDone = async (id) => {
    try {
      const response = await fetch(`http://localhost:5000/todos/done/${id}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({}),
      });

      setTodosChange(true);
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      <h3 className="mt-5">Task to do:</h3>
      <table className="table text-center">
        <thead>
          <tr>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
            <th>Mark as Done</th>
          </tr>
        </thead>
        <tbody>
          {todoTask.map((todo) => (
            <tr key={todo.todo_id}>
              <td>{todo.description}</td>
              <td>
                <EditTodo todo={todo} setTodosChange={setTodosChange} />
              </td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => deleteTodo(todo.todo_id)}
                >
                  Delete
                </button>
              </td>
              <td>
                <button
                  className="btn btn-success"
                  onClick={(e) => markAsDone(todo.todo_id)}
                >
                  Done
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Fragment>
  );
};

export default ListTodo;
